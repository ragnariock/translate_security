from model import * 
from create_dataest import *
from train import * 


import string
import random
import torch
import torch.nn as nn
import torch.optim as optim

import torchtext
from torchtext.datasets import TranslationDataset, Multi30k
from torchtext.data import Field, BucketIterator

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

import spacy
import numpy as np

import random
import math
import time



def main_train(name_of_model:str, order=["EC"], path_train_dataset = "train.csv",path_val_dataset = "val.csv", LEARNING_RATE = 0.0005,N_EPOCHS = 10, CLIP = 1):


    generate_object = generate_csv(path_train_dataset, path_val_dataset)
    generate_object.create_csv()
    train_iterator, valid_iterator = generate_object.create_iterator()


    print("FINISH PREPARE DATA ")
    if order == ["EC"]:  
        model = build_model(generate_object.SRC, generate_object.TRG, generate_object.device)
    elif order == ["CE"]:
        model = build_model(generate_object.TRG, generate_object.SRC, generate_object.device)
    else:
        raise Exception("HELLO MY FRIEND !")

    print("FINISH MODEL ")
    model.apply(initialize_weights)


    if order == ["EC"]:  
        TRG_PAD_IDX = generate_object.TRG.vocab.stoi[generate_object.TRG.pad_token]
    elif order == ["CE"]:
        TRG_PAD_IDX = generate_object.SRC.vocab.stoi[generate_object.SRC.pad_token]
    else:
        raise Exception("HELLO MY FRIEND !")

    optimizer = torch.optim.Adam(model.parameters(), lr = LEARNING_RATE)

    criterion = nn.CrossEntropyLoss(ignore_index = TRG_PAD_IDX)


    best_valid_loss = float('inf')
    print("Start  training !  ")
    for epoch in range(N_EPOCHS):
        
        start_time = time.time()
        
        train_loss = train(model, train_iterator, optimizer, criterion, CLIP, order)
        #valid_loss = evaluate(model, valid_iterator, criterion)
        
        end_time = time.time()
        
        epoch_mins, epoch_secs = epoch_time(start_time, end_time)
        
        # if valid_loss < best_valid_loss:
        #     best_valid_loss = valid_loss
        torch.save(model.state_dict(), name_of_model)
        
        print(f'Epoch: {epoch+1:02} | Time: {epoch_mins}m {epoch_secs}s')
        print(f'\tTrain Loss: {train_loss:.3f} | Train PPL: {math.exp(train_loss):7.3f}')
        # print(f'\t Val. Loss: {valid_loss:.3f} |  Val. PPL: {math.exp(valid_loss):7.3f}')
import string
import random
import spacy
from torchtext.data import Field, BucketIterator, TabularDataset
from sklearn.model_selection import train_test_split# create train and validation set 
import torchtext
import torch
import pandas as pd


class CreateDataset:
    def __init__(self, path_to_en_file, path_to_generate_dataset,
                path_to_letters, number=2):
        self.path_to_en_file = path_to_en_file
        self.path_to_generate_dataset = path_to_generate_dataset
        self.path_to_letters = path_to_letters

        self.number=number

        self.code_letters = self.get_code_latters()
        self.english_latters = self.get_eng_letters()
        self.code_letters = self.code_letters[0:len(self.english_latters) + self.number]

        self.attetion = self.get_attention(self.number, self.english_latters, self.code_letters)
        self.attetion = self.attetion['attention']
    
    def get_code_latters(self):
        data_read = open(self.path_to_letters, "r")
        latters = data_read.readlines()
        latters_new=[]
        for i in latters:
            i = i.replace("\n","")
            latters_new.append(i)
        return latters_new
    
    def get_eng_letters(self):
        english_alphabet = string.ascii_lowercase
        return [str(i) for i in english_alphabet]

    def get_attention(self, number, alph1, alph2):
        dict_of_alphabet = {"EN":alph1,"CH":alph2, "attention":[]}
        attention=[]
        if len(alph1) < len(alph2):
            for i in range(len(alph1)):
                attention.append([alph1[i], alph2[i]])
            for i in range(len(alph2) -len(alph1)):
                random_number = random.randint(0, len(alph1)-1)
                attention[random_number].append(alph2[i])
        else : 
            raise Exception("please chaek ordering of aphabets ! ")
        dict_of_alphabet['attention'].extend(attention)
        return dict_of_alphabet
    
    def create_dataset(self, attention, list_of_en):
        global_array=[]
        for i, batch in enumerate(list_of_en):
            new_batch = batch.lower()
            for litearl in new_batch:
                for check in attention:
                    if len(check) == 2 : 
                        if check[0] == litearl:
                            new_batch = new_batch.replace(litearl, check[1],1)
                    if len(check) == 3 : 
                        if check[0] == litearl:
                            new_batch = new_batch.replace(litearl, check[random.randint(1,2)],1)
            global_array.append(new_batch)
        return global_array
    
    def load_text_eng(self):
        data_read = open(self.path_to_en_file, "r")
        data_en = data_read.readlines()
        return data_en
    
    def write_txt_file(self, result_of_list):
        with open(self.path_to_generate_dataset, "w") as f :
            for i,batch in enumerate(result_of_list):
                f.write(batch)

    def main(self):
        data_eng = self.load_text_eng()
        hello = self.create_dataset(self.attetion, data_eng)
        self.write_txt_file(hello)
        return hello



class generate_csv(CreateDataset):
    def __init__(self,path_to_save_train_csv,
                    path_to_save_val_csv,BATCH_SIZE = 128):
        self.path_to_save_train_csv=path_to_save_train_csv
        self.path_to_save_val_csv=path_to_save_val_csv
        self.BATCH_SIZE = BATCH_SIZE

        self.path_to_en_file = "dataset/train.txt"
        self.path_to_save_generate = "dataset/geenrated.txt"
        self.path_to_letters = "dataset/forign.txt"
        CreateDataset.__init__(self, path_to_en_file=self.path_to_en_file,
        path_to_generate_dataset=self.path_to_save_generate,
        path_to_letters=self.path_to_letters)
        super().main()

        self.europarl_fr = open(self.path_to_save_generate).read().split('\n')
        self.europarl_en = open(self.path_to_en_file).read().split('\n')
        
        self.en = spacy.load('en')
        self.fr = spacy.load('fr')

        self.SRC = Field(tokenize = self.tokenize_en, 
            init_token = '<sos>', 
            eos_token = '<eos>', 
            lower = True, 
            batch_first = True)

        self.TRG = Field(tokenize = self.tokenize_fr, 
                    init_token = '<sos>', 
                    eos_token = '<eos>', 
                    lower = True, 
                    batch_first = True)
        
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    
    def tokenize_en(self, sentence):
        return [tok.text for tok in self.en.tokenizer(sentence)]

    def tokenize_fr(self, sentence):
        return [tok.text for tok in self.fr.tokenizer(sentence)]
    

    def create_csv(self):
        raw_data = {'English' : [line for line in self.europarl_en], 'Frign': [line for line in self.europarl_fr]}
        df = pd.DataFrame(raw_data, columns=["English", "Frign"])# remove very long sentences and sentences where translations are 
        df['fr_len'] = df['Frign'].str.count(' ')

        train, val = train_test_split(df, test_size=0.1)
        train.to_csv(self.path_to_save_train_csv, index=False)
        val.to_csv(self.path_to_save_val_csv, index=False)

    def create_iterator(self):
        data_fields = [('English', self.SRC), ('French', self.TRG)]
        train,val = torchtext.data.TabularDataset.splits(path='./', train=self.path_to_save_train_csv, validation=self.path_to_save_val_csv, format='csv', fields=data_fields, skip_header = True)
        
        self.SRC.build_vocab(train, min_freq = 2)
        self.TRG.build_vocab(train, min_freq = 2)

        train_iterator, valid_iterator = BucketIterator.splits(
            (train, val), 
            batch_size = self.BATCH_SIZE,
            device = self.device)
        return train_iterator, valid_iterator

